package cni.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import cni.entities.Activite;
import cni.entities.DureeP;
import cni.entities.TypeC;

import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Contrat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id ;
    private Date dateC ;
    @Enumerated(EnumType.STRING)
    private TypeC typeC ;
    @Enumerated(EnumType.STRING)
    private Activite activite ;
    private Boolean renouvable ;
    private Date dateD ;
    private Date dateF ;
    @Enumerated(EnumType.STRING)
    private DureeP dureeP ;
    private double montant ;
    private double taux_aug ;
}
