package cni.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Bien {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id ;
    @Enumerated(EnumType.STRING)
    private TypeBien typeBien ;
    private String description ;
    @Enumerated(EnumType.STRING)
    private Etat etat ;
    private int repeteur ;
    private String diviseur ;
    private int num_rep ;
    private int cp ;
    @ManyToOne
    private Cercle cercle ;
}
