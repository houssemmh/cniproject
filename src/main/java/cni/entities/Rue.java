package cni.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Rue {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id ;
    private String nom ;
    @Enumerated(EnumType.STRING)
    private TypeRue typeRue ;
    @ManyToOne
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)

    private Cercle cercles ;
}
