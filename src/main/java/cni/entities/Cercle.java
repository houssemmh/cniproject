package cni.entities;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Cercle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id ;
    private String nom ;
    @OneToMany(mappedBy = "cercles")
    private Collection<Rue> rues;
    @OneToMany(mappedBy = "cercle")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Collection<Bien>biens;
}
