package cni.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import cni.entities.Rue;

public interface RueRepository extends JpaRepository<Rue, Long> {

}
