package cni.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import cni.entities.Bien;

public interface BienRepository extends JpaRepository<Bien, Long>{

}
