package cni.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import cni.entities.Cercle;

public interface CercleRepository extends JpaRepository<Cercle, Long> {

}
