package cni.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import cni.entities.Contrat;

public interface ContratRepository extends JpaRepository<Contrat, Long> {

}
