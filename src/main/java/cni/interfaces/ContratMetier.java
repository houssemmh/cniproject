package cni.interfaces;

import java.util.List;

import cni.entities.Contrat;

public interface ContratMetier {
	Contrat addContrat (Contrat contrat);
	Contrat editContrat (Contrat contrat , Long id );
	void deleteContrat (Long id);
	List<Contrat> Contrat_list();
	

}
