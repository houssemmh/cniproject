package cni.interfaces;

import java.util.List;

import cni.entities.Rue;

public interface RueMetier {
	Rue addRue (Rue rue );
	Rue editRue(Rue rue , Long id );
	void deleteRue (Long id);
	List<Rue> Rue_List();

}
