package cni.interfaces;

import java.util.List;

import cni.entities.Cercle;

public interface CercleMetier {
	Cercle addCercle(Cercle cercle);
	Cercle editCercle(Cercle cercle , Long id);
	void deleteCercle(Long id );
	List<Cercle> Cercle_List();
	


}
