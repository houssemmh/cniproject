package cni.interfaces;

import java.util.List;

import cni.entities.Bien;

public interface BienMetier {
	Bien addBien(Bien bien);
	Bien editBien(Bien bien , Long id );
	void deleteBien(Long id );
	List<Bien> Bien_List();
	

}
