package cni;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CniProjectFinalApplication {

	public static void main(String[] args) {
		SpringApplication.run(CniProjectFinalApplication.class, args);
	}

}
