package cni.services;

import java.util.List;

import cni.interfaces.CercleMetier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cni.entities.Cercle;
import cni.repositories.CercleRepository;

@Service
public class CercleMetierImp implements CercleMetier {
	
	@Autowired
	private CercleRepository cercleRepository ; 
	@Override
	public Cercle addCercle(Cercle cercle) {
		return cercleRepository.save(cercle);
	}

	@Override
	public Cercle editCercle(Cercle cercle, Long id) {
		cercle.setId(id);
		return cercleRepository.save(cercle);
	}

	@Override
	public void deleteCercle(Long id) {
		cercleRepository.deleteById(id);
	}

	@Override
	public List<Cercle> Cercle_List() {
		return cercleRepository.findAll();
	}
	

}
