package cni.services;

import java.util.List;

import cni.interfaces.RueMetier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cni.entities.Rue;
import cni.repositories.RueRepository;
@Service
public class RueMetierImp implements RueMetier {
	@Autowired
	private RueRepository rueRepository ; 
	
	@Override
	public Rue addRue(Rue rue) {
		return rueRepository.save(rue);
	}

	@Override
	public Rue editRue(Rue rue, Long id) {
		rue.setId(id);
		return rueRepository.save(rue);
	}

	@Override
	public void deleteRue(Long id) {
		rueRepository.deleteById(id);
		
	}

	@Override
	public List<Rue> Rue_List() {
		return rueRepository.findAll();
	}

}
