package cni.services;

import java.util.List;

import cni.interfaces.ContratMetier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cni.entities.Contrat;
import cni.repositories.ContratRepository;
@Service
public class ContratMetierImp implements ContratMetier {
	@Autowired
	private ContratRepository contratRepository ; 
	
	@Override
	public Contrat addContrat(Contrat contrat) {
		return contratRepository.save(contrat);
	}

	@Override
	public Contrat editContrat(Contrat contrat, Long id) {
		contrat.setId(id);
		return contratRepository.save(contrat);
	}

	@Override
	public void deleteContrat(Long id) {
		contratRepository.deleteById(id);
		
	}

	@Override
	public List<Contrat> Contrat_list() {
		return contratRepository.findAll();
	}

}
