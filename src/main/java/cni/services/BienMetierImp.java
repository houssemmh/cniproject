package cni.services;

import java.util.List;

import cni.interfaces.BienMetier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cni.entities.Bien;
import cni.repositories.BienRepository;

@Service
public class BienMetierImp implements BienMetier {
	@Autowired
	private BienRepository bienRepository ;
	
	@Override
	public Bien addBien(Bien bien) {
		return bienRepository.save(bien);
	}

	@Override
	public Bien editBien(Bien bien, Long id) {
		bien.setId(id);
		return bienRepository.save(bien);
	}

	@Override
	public void deleteBien(Long id) {
		bienRepository.deleteById(id);
		
	}

	@Override
	public List<Bien> Bien_List() {
		return bienRepository.findAll();
	}

}
