package cni.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import cni.entities.Rue;
import cni.interfaces.RueMetier;

@RestController
public class RueRestController {
	@Autowired
	private RueMetier rueMetier ; 
	
	@PostMapping(value="/cni/rue")
	public Rue addRue(@RequestBody Rue rue){
		return rueMetier.addRue(rue);
	}
	
	@GetMapping(value="/cni/rues")
	public List<Rue> rueList(){
		return rueMetier.Rue_List();
	}
	
	 @PutMapping(value="/cni/rue/{id}")
	 public Rue editRue(@RequestBody Rue rue , @PathVariable(name ="id") Long id ){
		 return rueMetier.editRue(rue, id);
	 }
	 
	 @DeleteMapping(value="/cni/rue/{id}")
	 public void deleteRue(@PathVariable(name="id")Long id ){
		 rueMetier.deleteRue(id);
	 }

}
