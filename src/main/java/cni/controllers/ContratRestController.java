package cni.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import cni.entities.Contrat;

import cni.interfaces.ContratMetier;

@RestController
public class ContratRestController {
	@Autowired
	private ContratMetier contratMetier ; 
	
	@PostMapping(value="/cni/contrat")
	public Contrat addContrat(@RequestBody Contrat contrat){
		return contratMetier.addContrat(contrat);
	}
	
	@GetMapping(value="/cni/contrats")
	public List<Contrat> contratList(){
		return contratMetier.Contrat_list();
	}
	
	 @PutMapping(value="/cni/contrat/{id}")
	 public Contrat editContrat(@RequestBody Contrat contrat , @PathVariable(name ="id") Long id ){
		 return contratMetier.editContrat(contrat, id);
	 }
	 
	 @DeleteMapping(value="/cni/contrat/{id}")
	 public void deleteContrat(@PathVariable(name="id")Long id ){
		  contratMetier.deleteContrat(id);
	 }


}
