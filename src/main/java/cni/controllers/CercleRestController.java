package cni.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import cni.entities.Cercle;
import cni.interfaces.CercleMetier;

@RestController
public class CercleRestController {
	@Autowired
	private CercleMetier cercleMetier ; 
	
	@PostMapping(value="/cni/cercle")
	public Cercle addCercle(@RequestBody Cercle cercle){
		return cercleMetier.addCercle(cercle);
	}
	
	@GetMapping(value="/cni/cercles")
	public List<Cercle> cercleList(){
		return cercleMetier.Cercle_List() ;
	}
	
	 @PutMapping(value="/cni/cercle/{id}")
	 public Cercle editCercle(@RequestBody Cercle cercle , @PathVariable(name ="id") Long id ){
		 return cercleMetier.editCercle(cercle, id);
	 }
	 
	 @DeleteMapping(value="/cni/cercle/{id}")
	 public void deleteCercle(@PathVariable(name="id")Long id ){
		  cercleMetier.deleteCercle(id);
	 }

}
