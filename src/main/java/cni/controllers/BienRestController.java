package cni.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import cni.entities.Bien;
import cni.interfaces.BienMetier;

@RestController
public class BienRestController {
	@Autowired
	private BienMetier bienMetier ; 
	
	@PostMapping(value="/cni/bien")
	public Bien addBien(@RequestBody Bien bien){
		return bienMetier.addBien(bien);
	}
	
	@GetMapping(value="/cni/biens")
	public List<Bien> bienList(){
		return bienMetier.Bien_List() ;
	}
	
	 @PutMapping(value="/cni/bien/{id}")
	 public Bien editBien(@RequestBody Bien bien , @PathVariable(name ="id") Long id ){
		 return bienMetier.editBien(bien, id);
	 }
	 
	 @DeleteMapping(value="/cni/bien/{id}")
	 public void deleteBien(@PathVariable(name="id")Long id ){
		  bienMetier.deleteBien(id);
	 }
	 
}
